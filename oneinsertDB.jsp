<meta http-equiv="content-Type" content="text/html; charset=utf-8" />

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<%	
	request.setCharacterEncoding("UTF-8");
	
	int kor;
	String kors = request.getParameter("kor");
	kor = Integer.parseInt(kors);
	
	int mat;
	String mats = request.getParameter("mat");
	mat = Integer.parseInt(mats);
	
	int eng;
	String engs = request.getParameter("eng");
	eng = Integer.parseInt(engs);
	
	String name = request.getParameter("name");
%>
<%!		
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
<style>
.head {
	margin-top : 50px;
}
</style>
</head>
<body>
<div class="head">
<h1>성적 데이터 입력 추가 완료</h1>

<%	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	try{
	ResultSet rs = stmt.executeQuery("select min(studentid+1) from examtable where (studentid+1) not in (select studentid from examtable);");
	int addstudentid = 0;
	while(rs.next()){
		addstudentid=rs.getInt(1);
	}		
	stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('"+name+"',"+addstudentid+","+kor+","+eng+","+mat+");");
	out.println("<table border=1 width=350 cellspacing=0>");
	out.println("<tr>");
	out.println("<td align=center>이름</td>");
	out.println("<td><input type=text value="+name+">"+"</input></td>");
	out.println("</tr>");
	out.println("<tr>");
	out.println("<td align=center>학번</td>");
	out.println("<td><input type = text value=" + addstudentid + ">" +"</input></td>");
	out.println("</tr>");
	out.println("<tr>");
	out.println("<td align=center>국어</td>");
	out.println("<td><input type = text value="+kor + ">" +"</input></td>");
	out.println("</tr>");
	out.println("<tr>");
	out.println("<td align=center>영어</td>");
	out.println("<td><input type = text value="+eng + ">" +"</input></td>");	
	out.println("</tr>");
	out.println("<tr>");
	out.println("<td align=center>수학</td>");
	out.println("<td><input type = text value="+mat + ">" +"</input></td>");	
	out.println("</tr>");
	out.println("</table>");
	out.println("<form method=post action=inputForm1.html>");
	out.println("<input type=submit value=뒤로가기></input>");
	out.println("</form>");
	rs.close();
	} catch(SQLException e) {
		out.println(extractErrorCode(e)+" 에러가 발생했습니다. <br>");
		out.println("테이블이 존재하지 않습니다.");
	}
	stmt.close();
	conn.close();
%>
</div>
</body>
</html>