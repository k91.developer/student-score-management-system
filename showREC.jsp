<meta http-equiv="content-Type" content="text/html; charset=utf-8" />

<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<%
	int id;
	String ids = request.getParameter("id");
	id = Integer.parseInt(ids);
	
%>
<%!		
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
<style>
.head {
	margin-top : 50px;
}
</style>
</head>
<body>
<div class="head">
<h1> 성적 조회후 정정/삭제</h1>

<%	
	try{
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	ResultSet rset = stmt.executeQuery("select * from examtable where studentid ="+ id +";");
	
	String name="해당학번없음";
	String studentid="";
	String kor="";
	String eng="";
	String mat="";
	
	while (rset.next()) {
		name=rset.getString(1);
		studentid=Integer.toString(rset.getInt(2));
		kor=Integer.toString(rset.getInt(3));
		eng=Integer.toString(rset.getInt(4));
		mat=Integer.toString(rset.getInt(5));		
	}
	rset.close();
	stmt.close();
	conn.close();
		out.println("<form method=post action=updateDB.jsp>");
		out.println("<table cellspacing=0 width=350 border=1>");
		out.println("<tr>");
		out.println("<td width='30%' align=center>이름</td>");
		out.println("<td width='70%'><input type=text name=stname value=" + name + ">" + "</input></td>");
		out.println("<tr>");
		out.println("<td align=center>" + "학번" + "</td>");
		out.println("<td><input type=text readonly name=id value=" + studentid + ">" + "</input></td>");
		out.println("<tr>");
		out.println("<td align=center>" + "국어" + "</td>");
		out.println("<td><input type=number min=0 max=100 name=kor value=" + kor + ">" + "</input></td>");
		out.println("<tr>");
		out.println("<td align=center>" + "영어" + "</td>");
		out.println("<td><input type=number min=0 max=100 name=eng value=" + eng + ">" + "</input></td>");
		out.println("<tr>");
		out.println("<td align=center>" + "수학" + "</td>");
		out.println("<td><input type=number min=0 max=100 name=mat value=" + mat + ">" + "</input></td>");
		out.println("<tr>");
		out.println("</table>");
	if(name.equals("해당학번없음")){
	}else{
		out.println("<br>");
		out.println("<table cellspacing=1 width=350 border=0>");
		out.println("<input type=submit value=수정 ></input>");
		out.println("</form>");	
		out.println("<form method=post action=deleteDB.jsp?id="+studentid+"&name="+name+">");
		out.println("<input type=submit value=삭제></input>");
		out.println("</form>");
		out.println("</table>");
	}
	out.println("</table>");
	} catch(SQLException e) {
		out.println(extractErrorCode(e)+" 에러가 발생했습니다. <br>");
		out.println("테이블이 존재하지 않습니다.");
	}
%>
</div>
</body>
</html>