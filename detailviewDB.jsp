<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<%!
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
<%
	request.setCharacterEncoding("UTF-8");
	
	int fromPT;
	String fromPTs = request.getParameter("fromPT");
	try{
		fromPT = Integer.parseInt(fromPTs);
	}catch (Exception e) {
		fromPT=0;
	}
	
	int cntPT;
	String cntPTs = request.getParameter("cntPT");
	try{
		cntPT = Integer.parseInt(cntPTs);
	}catch (Exception e) {
		cntPT=5;
	}
%>
<style>
.head {
	margin-top : 50px;
}
.page {
	text-decoration : none;
}
.act {
	text-decoration : none;
	color : skyblue;
}
</style>
</head>
<body>
<div class="head">
<h1>데이터 상세 조회</h1>

<%
	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	
	try {
	ResultSet rset = stmt.executeQuery("select name, studentid, kor, eng, mat, (kor+mat+eng) as sum, round((kor+mat+eng)/3,1) as avg, "+
									   "(select count(*)+1 from examtable where kor+eng+mat>t.kor+t.eng+t.mat) as rank from examtable t"+
									   " group by name, studentid, kor, eng, mat order by rank;");
	ResultSet rs = stmt2.executeQuery("select count(*) from examtable;");
	int rows=0;
	int cnt=0;
	int current;
	int pages;
	
	while (rs.next()){
		rows = rs.getInt(1);
	}
		out.println("<table cellspacing=0 width=600 border=1>");
		out.println("<tr>");
		out.println("<td width=50><p align=center>" + "번호" + "</a></p></td>");
		out.println("<td width=50><p align=center>" + "이름" + "</a></p></td>");
		out.println("<td width=50><p align=center>" + "학번" + "</p></td>");
		out.println("<td width=50><p align=center>" + "국어" + "</p></td>");
		out.println("<td width=50><p align=center>" + "영어" + "</p></td>");
		out.println("<td width=50><p align=center>" + "수학" + "</p></td>");
		out.println("<td width=50><p align=center>" + "합계" + "</p></td>");
		out.println("<td width=50><p align=center>" + "평균" + "</p></td>");
		out.println("<td width=50><p align=center>" + "순위" + "</p></td>");
		out.println("</tr>");
	if(rows!=0){
		while (rset.next()) {
			if(fromPT>cnt) {
				cnt++;
				continue;
			}else if(cnt>=(fromPT+cntPT)){
				break;
			}
			out.println("<tr>");
			out.println("<td><p align=center>" + (cnt+1) + "</p></td>");
			out.println("<td><p align=center><a href=onedetailviewDB.jsp?id="+rset.getInt(2)+"&name="+rset.getString(1)+" target=main>" 
																			 + rset.getString(1) + "</a></p></td>");
			out.println("<td><p align=center>" + Integer.toString(rset.getInt(2)) + "</p></td>");
			out.println("<td><p align=center>" + Integer.toString(rset.getInt(3)) + "</p></td>");
			out.println("<td><p align=center>" + Integer.toString(rset.getInt(4)) + "</p></td>");
			out.println("<td><p align=center>" + Integer.toString(rset.getInt(5)) + "</p></td>");
			out.println("<td><p align=center>" + Integer.toString(rset.getInt(6)) + "</p></td>");
			out.println("<td><p align=center>" + Double.toString(rset.getDouble(7)) + "</p></td>");
			out.println("<td><p align=center>" + Integer.toString(rset.getInt(8)) + "</p></td>");
			out.println("</tr>");
			cnt++;
		}
		
		if(rows%cntPT==0){
			pages=rows/cntPT;
		}else{
			pages=(rows/cntPT)+1;
		}
		current=fromPT/cntPT+1;
		
		out.println("</table>");
		out.println("<br><table cellspacing=0 width=600 border=0>");
		out.println("<tr>");
		out.println("<td align=center colspan=9><a class=page href=detailviewDB.jsp?fromPT="+0+"&cntPT="+cntPT+">"+ "<<" + "</a>");
		for (int i = 0; i < pages; i++) {
			if(current==(i+1)){
				out.println("<a class='page act' href=detailviewDB.jsp?fromPT="+(i*cntPT)+"&cntPT="+cntPT+"> ["+ (i+1) + "]</a>");
			}else{
				out.println("<a class=page href=detailviewDB.jsp?fromPT="+(i*cntPT)+"&cntPT="+cntPT+"> "+ (i+1) + "</a>");
			}
		}
		out.println("<a class=page href=detailviewDB.jsp?fromPT="+((cntPT*(pages-1)))+"&cntPT="+cntPT+">"+ ">>" + "</a></td>");
		out.println("</tr>");
	
	}else{
		out.println("<tr>");
		out.println("<td width=50 colspan=9><p align=center>데이터가 없음</p></td>");
		out.println("</tr>");
	}
	out.println("</table>");
	
	rs.close();
	rset.close();
	stmt2.close();
	stmt.close();
	conn.close();
	
	} catch(SQLException e) {
			if(extractErrorCode(e)==1146){
				out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
				out.println("테이블이 존재하지 않습니다.");
			}else{
				out.println("[기타] <br>");
				out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
			}
			
	}
%>

</div>
</body>
</html>