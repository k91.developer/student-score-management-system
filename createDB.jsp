<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<style>
.head {
	margin-top : 50px;
}
</style>
</head>
<body>
<div class="head">
<h1>테이블 생성</h1>

<%	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	try{
	stmt.execute("create table examtable(name varchar(20), studentid int not null primary key,"+ 
										 "kor int, eng int, mat int) default charset=utf8;");
	out.println("테이블이 생성되었습니다.");
	} catch (SQLException e) {
		out.println("테이블 이름이 중복됩니다. 다른 테이블 이름을 입력하세요.");
	}
	stmt.close();
	conn.close();
%>
</div>
</body>
</html>