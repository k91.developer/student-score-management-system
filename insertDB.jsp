<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<%!
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
<style>
.head {
	margin-top : 50px;
}
</style>
</head>
<body>
<div class="head">
<h1>데이터 입력</h1>

<%	
	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	try{
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('나연', 209901, 95, 100, 95);");
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('정연', 209902, 100, 100, 100);");
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('모모', 209903, 100, 95, 100);");
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('사나', 209904, 100, 95, 90);");
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('지효', 209905, 80, 100, 70);");
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('미나', 209906, 95, 90, 95);");
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('다현', 209907, 100, 90, 100);");
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('채영', 209908, 100, 75, 90);");
		stmt.execute("insert into examtable (name, studentid, kor, eng, mat) values ('쯔위', 209909, 100, 100, 70);");
		
		ResultSet rs = stmt.executeQuery("select count(*) from examtable;");
		while(rs.next()) {
			out.println(""+rs.getInt(1)+"건의 데이터가 입력되었습니다.");
		}
		rs.close();
	} catch(SQLException e) {
		if(extractErrorCode(e)==1062){
			out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
			out.println("데이터가 중복됩니다.");
		}else if(extractErrorCode(e)==1146){
			out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
			out.println("테이블이 존재하지 않습니다.");
		}else{
			out.println("[기타] <br>");
			out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
		}			
	}
	stmt.close();
	conn.close();
%>

</div>
</body>
</html>