<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<%!
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
<style>
.head {
	margin-top : 50px;
}
</style>
</head>
<body>
<div class="head">
<h1>데이터 조회</h1>

<%	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	try {
	ResultSet rset = stmt.executeQuery("select * from examtable;");
	ResultSet rs = stmt2.executeQuery("select count(*) from examtable;");
	int rows=0;
	while (rs.next()){
		rows = rs.getInt(1);
	}
		out.println("<table cellspacing=0 width=600 border=1>");
		out.println("<tr>");
		out.println("<td width=20% align=center> 이름 </td>");
		out.println("<td width=20% align=center> 학번 </td>");
		out.println("<td width=20% align=center> 국어 </td>");
		out.println("<td width=20% align=center> 영어 </td>");
		out.println("<td width=20% align=center> 수학 </td>");
		out.println("</tr>");
	if(rows!=0){
		while (rset.next()) {
			out.println("<tr>");
			out.println("<td align=center><a href=oneviewDB.jsp?id="+rset.getInt(2)+"&name="+rset.getString(1)+" target=main>" + rset.getString(1) + "</a></td>");
			out.println("<td align=center>" + Integer.toString(rset.getInt(2)) + "</td>");
			out.println("<td align=center>" + Integer.toString(rset.getInt(3)) + "</td>");
			out.println("<td align=center>" + Integer.toString(rset.getInt(4)) + "</td>");
			out.println("<td align=center>" + Integer.toString(rset.getInt(5)) + "</td>");
			out.println("</tr>");
		}
	}else{
		out.println("<tr>");
		out.println("<td colspan=5><p align=center>데이터가 없음</p></td>");
		out.println("</tr>");
	}
	out.println("</table>");
	rs.close();
	rset.close();
	stmt2.close();
	stmt.close();
	conn.close();
	} catch(SQLException e) {
			if(extractErrorCode(e)==1146){
				out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
				out.println("테이블이 존재하지 않습니다.");
			}else{
				out.println("[기타] <br>");
				out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
			}			
	}
%>

</div>
</body>
</html>