<meta http-equiv="content-Type" content="text/html; charset=utf-8" />

<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<%
	int id;
	String ids = request.getParameter("id");
	id = Integer.parseInt(ids);
	
	String name = request.getParameter("name");
		
%>
<style>
.head {
	margin-top : 50px;
}
</style>
</head>
<body>
<div class="head">
<h1>[<%=name%>] 데이터 상세 조회</h1>

<%	
	
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	ResultSet rset = stmt.executeQuery("select name, studentid, kor, eng, mat, (kor+mat+eng) as sum, round((kor+mat+eng)/3,1) as avg, "+
									   "(select count(*)+1 from examtable where kor+eng+mat>t.kor+t.eng+t.mat) as rank "+
									   "from examtable t group by name, studentid, kor, eng, mat having studentid ="+ id +";");

		out.println("<table cellspacing=0 width=600 border=1>");
		out.println("<tr>");
		out.println("<td width=50 align=center> 이름 </td>");
		out.println("<td width=50 align=center> 학번 </td>");
		out.println("<td width=50 align=center> 국어 </td>");
		out.println("<td width=50 align=center> 영어 </td>");
		out.println("<td width=50 align=center> 수학 </td>");
		out.println("<td width=50><p align=center>" + "합계" + "</p></td>");
		out.println("<td width=50><p align=center>" + "평균" + "</p></td>");
		out.println("<td width=50><p align=center>" + "순위" + "</p></td>");
		out.println("</tr>");
	while (rset.next()) {
		out.println("<tr>");
		out.println("<td width=50><p align=center>" + rset.getString(1) + "</p></td>");
		out.println("<td width=50><p align=center>" + Integer.toString(rset.getInt(2)) + "</p></td>");
		out.println("<td width=50><p align=center>" + Integer.toString(rset.getInt(3)) + "</p></td>");
		out.println("<td width=50><p align=center>" + Integer.toString(rset.getInt(4)) + "</p></td>");
		out.println("<td width=50><p align=center>" + Integer.toString(rset.getInt(5)) + "</p></td>");
		out.println("<td width=50><p align=center>" + Integer.toString(rset.getInt(6)) + "</p></td>");
		out.println("<td width=50><p align=center>" + Double.toString(rset.getDouble(7)) + "</p></td>");
		out.println("<td width=50><p align=center>" + Integer.toString(rset.getInt(8)) + "</p></td>");
		out.println("</tr>");
	}
	rset.close();
	stmt.close();
	conn.close();
	
%>
</table>
</div>
</body>
</html>